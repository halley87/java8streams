import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class DistinctTest {
    @Test
    public void distinct() {
        final List<Integer> numbers = ImmutableList.of(1,1,1,2,3,3,3,3,2,2,2,4,5,5,5,5,5,4);
        List<Integer> distinctNumbers = numbers.stream()
                .distinct()
                .collect(Collectors.toList());

        assertThat(distinctNumbers).hasSize(5);
        System.out.println(distinctNumbers);
    }

    @Test
    public void distinctWithSet() {
        final List<Integer> numbers = ImmutableList.of(1,2,2,1,2,1,2,2,3,3,3,4,4,5,5,5);
        Set<Integer> distinctNumbers = numbers.stream()
                .collect(Collectors.toSet());

        assertThat(distinctNumbers).hasSize(5);
        System.out.println(distinctNumbers);
    }
}
