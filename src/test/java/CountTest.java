import beans.Car;
import mockdata.MockData;
import org.junit.Test;

import java.io.IOException;
import java.util.DoubleSummaryStatistics;

// getAsDouble
// orElse
public class CountTest {
    @Test
    public void count() throws IOException {
        long count = MockData.getPeople()
                .stream()
                .filter(person -> person.getGender().equalsIgnoreCase("male"))
                .count();
        System.out.println(count);
    }

    @Test
    public void min() throws IOException {
        double yellowCarWithMinPrice = MockData.getCars()
                .stream()
                .filter(car -> car.getColor().equalsIgnoreCase("yellow"))
                .mapToDouble(car -> car.getPrice())
                .min()
                .getAsDouble();
        System.out.println(yellowCarWithMinPrice);
    }

    @Test
    public void max() throws IOException {
        double yellowCarWithMaxPrice = MockData.getCars()
                .stream()
                .filter(car -> car.getColor().equalsIgnoreCase("yellow"))
                .mapToDouble(Car::getPrice)
                .max()
                .orElse(0d);
        System.out.println(yellowCarWithMaxPrice);
    }

    //saçma sapan bişeymiş
    @Test
    public void statistics() throws IOException {
        DoubleSummaryStatistics doubleSummaryStatistics = MockData.getCars()
                .stream()
                .mapToDouble(Car::getPrice)
                .summaryStatistics();
        System.out.println(doubleSummaryStatistics);
    }
}
