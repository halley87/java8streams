import beans.Car;
import beans.Person;
import beans.PersonDto;
import com.google.common.collect.ImmutableList;
import mockdata.MockData;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class FilterAdvancedTest {
    @Test
    public void understandingFilter() throws IOException {
        ImmutableList<Car> cars = MockData.getCars();
        List<Car> carsFiltered = cars.stream()
                .filter(car -> car.getPrice() < 200000)
                .collect(Collectors.toList());
        carsFiltered.forEach(System.out::println);
        System.out.println(carsFiltered.size());
    }

    @Test
    public void mapping() throws IOException {
        List<Person> people = MockData.getPeople();
        List<PersonDto> dtos = people.stream()
                .map(PersonDto::map)
                .collect(Collectors.toList());
        dtos.forEach(System.out::println);
        assertThat(dtos).hasSize(1000);
        System.out.println(dtos.size());
    }

    @Test
    public void averageCarPrice() throws IOException {
        double average = MockData.getCars()
                .stream()
                .mapToDouble(Car::getPrice)
                .average()
                .orElse(0);
        System.out.println(average);
    }
}
