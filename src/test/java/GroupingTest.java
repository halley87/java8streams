import beans.Car;
import mockdata.MockData;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GroupingTest {
    @Test
    public void simpleGrouping() throws IOException {
        Map<String, List<Car>> grouping = MockData.getCars()
                .stream()
                .collect(Collectors.groupingBy(Car::getMake));

        grouping.forEach((make, cars) -> {
            System.out.println(make);
            cars.forEach(System.out::println);
        });
    }

    @Test
    public void groupingAndCounting() throws IOException {
        Map<String, Long> countingByName = MockData.getPeople()
                .stream()
                .map(person -> person.getFirstName())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        countingByName.forEach((name, count) -> System.out.println(name + " > " + count));
    }
}
