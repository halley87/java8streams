import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.stream.Collectors;

public class JoinStringsWithStream {
    @Test
    public void joiningStringsWithStream() {
        ImmutableList<String> names = ImmutableList.of("anna", "john", "marcos", "helena", "yasmin");
        String join = names.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining("|"));
        System.out.println(join);
    }
}
