import org.junit.Test;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Predicate;

public class FindAnyTest {
    final Predicate<Integer> lessThanTen = n -> n < 10;

    // non-deterministic
    @Test
    public void findAny() {
        Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Integer any = Arrays.stream(numbers)
                .filter(lessThanTen)
                .findAny()
                .get();
        System.out.println(any);
    }

    // deterministic
    @Test
    public void findFirst() {
        Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int firstMatch = Arrays.stream(numbers)
                .filter(lessThanTen)
                .findFirst()
                .get();
        System.out.println(firstMatch);
    }
}
