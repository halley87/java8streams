import org.junit.Test;

import java.util.stream.IntStream;

public class LoopForSomeNumberTest {
    @Test
    public void range() {
        IntStream.rangeClosed(0, 10)
                .forEach(System.out::println);
    }

    @Test
    public void iterateForLimitedNumber() {
        IntStream.iterate(0, operand -> operand+1)
                .limit(20)
                .forEach(System.out::println);
    }
}
