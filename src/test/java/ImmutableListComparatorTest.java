import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ImmutableListComparatorTest {
    @Test
    public void min() {
        final List<Integer> numbers = ImmutableList.of(1,2,3,16,72,8,7,5);

        Integer min = numbers.stream()
                .min(Comparator.naturalOrder())
                .get();

        assertThat(min).isEqualTo(1);
        System.out.println(min);
    }
}
