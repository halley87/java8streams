import beans.Person;
import mockdata.MockData;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

public class UnderstandingCollect {
    @Test
    public void collect() throws IOException {
        ArrayList<String> emails = MockData.getPeople()
                .stream()
                .map(Person::getEmail)
                .collect(
                        ArrayList::new,
                        ArrayList::add,
                        ArrayList::addAll
                );

        emails.forEach(System.out::println);
    }
}
