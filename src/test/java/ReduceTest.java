import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ReduceTest {
    private static final List<ArrayList<String>> ARRAYLIST_OF_NAMES = Lists.newArrayList(
            Lists.newArrayList("Mariam", "Alex", "Ismail"),
            Lists.newArrayList("John", "Alesha", "Andre"),
            Lists.newArrayList("Susy", "Ali")
    );

    @Test
    @Before
    public void setUp() {
        System.out.println(ARRAYLIST_OF_NAMES);
    }

    @Test
    public void reduce() {
        Integer[] integers = {1, 2, 3, 4, 99, 100, 121, 1302, 199};

        int sum = Arrays.stream(integers).reduce(0, (a, b) -> a + b);
        System.out.println(sum);

        int sum2 = Arrays.stream(integers).reduce(0, Integer::sum);
    }

    @Test
    public void flatMap() {
        List<String> oneDimensionalList = ARRAYLIST_OF_NAMES.stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());
        System.out.println(oneDimensionalList);
    }
}
